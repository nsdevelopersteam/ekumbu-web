
var Client                      = require('node-rest-client').Client;
var config                      = require('../config');
const moment                    = require('moment')
var Q                           = require('q');
var client = new Client();

const PROFILEPAGEPATH           = 'profile';
const LOGINPAGE                 = 'signin';

var newEmailForChangeRequest = null;
var temporarySessionToken = null;

var smsconfirmation = 'checked';
var blocked = "checked";
var twowayauthStatus = 'checked';

function VerifyEmailValidate(emailParam) {
    var deferred = Q.defer();
    var args = {
        data: { 
            email: emailParam,
            livemode: "false",
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/chexeml", args, function (data, dataBasicInfo) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data);
        }
        else {
            deferred.reject(data);
        }
    });
    return deferred.promise;
}
function GetCardInfo(sessionToken) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/getccardinf", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo['message']);
        }
    })
    
    return deferred.promise;
}

function isEmptyObject(obj) {
    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
        return false;
        }
    }
    return true;
}

function GetCustomerPersonalInfoRequest(sessionToken) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/gcpinf", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo['message']);
        }
    })
    
    return deferred.promise;
}

function RequestChangePhoneRequest(sessionToken, newPhoneParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            newphone: newPhoneParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/requestchangephone", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function ConfirmRequestChangePhoneRequest(sessionToken, newPhoneParam, tokenParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            token: tokenParam,
            phone: newPhoneParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/confirmrequestchangephone", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function RequestChangeEmailRequest(sessionToken, newEmailParam, baseURLParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            urlpath: baseURLParam,
            newemail: newEmailParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/requestchangeemail", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function RequestChangeEmailTokenRequest(sessionToken, dpathParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            dpath: dpathParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/requestchangeemailtoken", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function ValidateRequestChangeEmailRequest(sessionToken, newEmailParam, tokenParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            token: tokenParam,
            email: newEmailParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/validaterequestchangeemail", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function RequestChangePasswordRequest(sessionToken, actualPasswordParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            currentpassword: actualPasswordParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/requestchangepassword", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function ValidateRequestChangePasswordRequest(sessionToken, newPasswordParam, tokenParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            token: tokenParam,
            newpassword: newPasswordParam,
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/validaterequestchangepassword", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

function ChangeTwoWaysAuthRequest(sessionToken, statusParam) {
    var deferred = Q.defer();

    var args = {
        data: { 
            sessiontoken : sessionToken,
            status: statusParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };
    client.patch(config.API.baseurl + "/api/v1/customer/changetwowaysauthstatus", args, function (ResponseData, response) {
        if(ResponseData['statusCode'] == 200) {
            deferred.resolve(ResponseData);
        }
        else {
            deferred.reject(ResponseData);
        }
    })
    
    return deferred.promise;
}

module.exports = {

    RenderProfilePage: function(request, reply) {

        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        
        GetCustomerPersonalInfoRequest(request.session.sessiontoken)
        .then(CustomerInfo => {

            twowayauthStatus = 'checked';
            smsconfirmation = 'checked';
            blocked = 'checked';

            if(!CustomerInfo['twowayauth']) {
                twowayauthStatus = "none";
            }

            if(!CustomerInfo['smsconfirmation']) {
                smsconfirmation = "none";
            }

            if(!CustomerInfo['blocked']) {
                blocked = "none";
            }

            dataR = {
                personalInfo : {
                    name : CustomerInfo['firstname'] + " " + CustomerInfo['lastname'],
                    email: CustomerInfo['email'],
                    phone : CustomerInfo['phone'],
                },
                security: {
                    smsconf: smsconfirmation,
                    blocked: blocked,
                    twowayauth: twowayauthStatus
                }
            }

            return reply.view('profile', dataR, { layout: 'generalDashboardOptLayout' });

        })
        .catch(err => {
            //TODO send to 500 page 
            console.log("3")
            console.log(err);
            request.session.logged = false;
            return reply.view('signin', {}, { layout: 'authLayout' });
        })
    },

    RequestChangePhoneNumber: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        else {
            RequestChangePhoneRequest(request.session.sessiontoken, request.payload.phone)
            .then(Information => {
                var infoResponse = {
                    statusCode: 200,
                    error : null,
                    message: 'success'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error : 'Internal Error',
                    message: error.message
                }
                return reply(infoResponse)
            })
        }
    },

    ConfirmChangePhoneNumber: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        else {
            ConfirmRequestChangePhoneRequest(request.session.sessiontoken, request.payload.phone, request.payload.token)
            .then(Information => {
                var infoResponse = {
                    statusCode: 200,
                    error : null,
                    message: PROFILEPAGEPATH
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                var infoResponse = {
                    statusCode: 500,
                    error : error.error,
                    message: error.message
                }
                return reply(infoResponse)
            })
        }
    },

    RequestChangeEmail: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        else {
            VerifyEmailValidate(request.payload.email)
            .then(Response => {
                if(!Response['result']) {
                    RequestChangeEmailRequest(request.session.sessiontoken, request.payload.email, config.server.development)
                    .then(Information => {
                        var infoResponse = {
                            statusCode: 200,
                            error : null,
                            message: 'success'
                        }
                        return reply(infoResponse)
                    })
                    .catch(error => {
                        var infoResponse = {
                            statusCode: 500,
                            error : 'Internal Error',
                            message: error.message
                        }
                        return reply(infoResponse)
                    })
                }
                else {
                    var infoResponse = {
                        statusCode: 403,
                        error : 'Request Error',
                        message: 'Já existe uma conta de email registrada com este email por favor escolha outro email.'
                    }
                    return reply(infoResponse)   
                }
            })
            .catch(error => {
                var infoResponse = {
                    statusCode: 500,
                    error : 'Internal Error',
                    message: 'Ocorreu um erro interno, por favor tente mais tarde.'
                }
                return reply(infoResponse)   
            })
        }
    },

    RenderRequestChangeEmailConfirmationPage: function(request, reply) {
        
        RequestChangeEmailTokenRequest(request.session.sessiontoken, request.params.dpath)
        .then(SuccessResponse => {

            temporarySessionToken = request.session.sessiontoken
            newEmailForChangeRequest = request.params.email;

            request.session = {};
            request.session.sessiontoken = null;
            request.session.logged = false;

            return reply.view('requestchangeemailconfirmation', {}, { layout: 'authLayout' });
        })
        .catch(error => {
            console.log(error)
            return reply.view(new Error('404'));
        })
    },

    ValidateRequestChangeEmail: function(request, reply) {
        ValidateRequestChangeEmailRequest(temporarySessionToken, newEmailForChangeRequest, request.payload.token)
        .then(SuccessResponse => {
            var infoResponse = {
                statusCode: 200,
                error : null,
                message: '/signin'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            var infoResponse = {
                statusCode: error.statusCode,
                error : error.error,
                message: error.message
            }
            return reply(infoResponse)
        })
    },

    RequestChangePassword: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        else {
            RequestChangePasswordRequest(request.session.sessiontoken, request.payload.actualpswd)
            .then(Information => {
                var infoResponse = {
                    statusCode: 200,
                    error : null,
                    message: 'success'
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                return reply(error)
            })
        }
    },

    ValidateChangePassword: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        else {
            ValidateRequestChangePasswordRequest(request.session.sessiontoken, request.payload.newpassword, request.payload.token)
            .then(Information => {
                var infoResponse = {
                    statusCode: 200,
                    error : null,
                    message: LOGINPAGE
                }
                return reply(infoResponse)
            })
            .catch(error => {
                console.log(error)
                var infoResponse = {
                    statusCode: 500,
                    error : error.error,
                    message: error.message
                }
                return reply(infoResponse)
            })
        }
    },

    ChangeTwoWaysAuthStatus: function(request, reply) {
        var boolStatus = false;

        if(request.payload.status === 'none') {
            boolStatus = true;
        }
        else {
            boolStatus = false;
        }

        ChangeTwoWaysAuthRequest(request.session.sessiontoken, boolStatus)
        .then(responseInfo => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: PROFILEPAGEPATH,
            }
            return reply(infoResponse)
        })
        .catch(ErrorResponse => {
            var infoResponse = {
                statusCode: ErrorResponse.statusCode,
                error: ErrorResponse.error,
                message: ErrorResponse.message,
            }
            return reply(infoResponse)
        })
    }
}