var Client                      = require('node-rest-client').Client;
var config                      = require('../config');
var Q                           = require('q');
var client = new Client();

var tranferTrackingID;

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function SendMoney(sessiontokenP, cardsn, amountP, ipaddressP, descriptionP) {
    var deferred = Q.defer();

    var args = {
        data: { 
            ipaddress : ipaddressP,
            description : descriptionP,
            dstCardN : cardsn,
            sessiontoken : sessiontokenP,
            livemode: config.API.livemode,
            amount: amountP
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.sendmoney.access_key
        }
    };
    client.post(config.sendmoney.url + "/sendmoney", args, function (TransferResponse, response) {
        if(TransferResponse['statusCode'] == 200) {
            deferred.resolve(TransferResponse);
        }
        else {
            deferred.reject(TransferResponse);
        }
    })
    
    return deferred.promise;
}

module.exports = {
    RenderSendMoney: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        return reply.view('sendmoney', {}, { layout: 'generalDashboardOptLayout' });   
    },

    RenderSendMoneySuccess: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(sendSuccesseded) {
            var data = {
                trackingID : tranferTrackingID
            }
            return reply.view('smsuccess', data, { layout: 'generalDashboardOptLayout' });  
        }
        else {
            return reply.view('sendmoney', {}, { layout: 'generalDashboardOptLayout' });
        } 
    },

    SendMoney: function(request, reply) {     
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        SendMoney(request.session.sessiontoken, request.payload.cardsn, request.payload.amount, request.info.remoteAddress, request.payload.description)
        .then(TransferCreated => {
            tranferTrackingID = TransferCreated['trackingID']
            sendSuccesseded = true
            var response = {
                statusCode: 200,
                error: null,
                message : 'smsuccess'
            }
            return reply(response);
        })
        .catch(err => {
            console.log(err)

            if(err.statusCode == 404) {
                var response = {
                    statusCode: 404,
                    error: null,
                    message : 'Numero do cartão do receptor inválido, verique se digitou correctamente'
                }
                return reply(response);   
            }
            else if(err.statusCode == 403) {
                if(err.message.toString() === 'declined[object Object]') {
                    var response = {
                        statusCode: 403,
                        error: null,
                        message : 'Não há fundos suficientes no seu cartão ekumbu para completar o envio'
                    }
                    return reply(response);
                }
            }
            else {
                var response = {
                    statusCode: 500,
                    error: null,
                    message : 'Occoreu um erro interno, por favor tente mais tarde'
                }
                return reply(response);  
            }
        })
    },
}