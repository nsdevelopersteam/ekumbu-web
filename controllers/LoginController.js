const Client                      = require('node-rest-client').Client;
const config                      = require('../config');
const randtoken                   = require('rand-token').generator();
const Q                           = require('q');

var client = new Client();
var randonTwoWaysURL = null;
var emailInUse = null;

function ValidateTwoWaysTokenRequest(emailParam, tokenParam) {
    
    var deferred = Q.defer();
    var args = {
        data: {
            token: tokenParam,
            email: emailParam,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.baseurl + "/api/v1/customer/validatetwowaysauth", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo);
        }
    })
    
    return deferred.promise;
}

var login = {

    LoginPage : function(request, reply) {
        request.session.logged = false;

        emailInUse = null;
        randonTwoWaysURL = null;

        var languageParam = null;
        
        request.session.page = "signin"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        return reply.view('signin', info, { layout: 'authLayout' });
    },

    Login: function(request, reply) {

        var languageParam = null;
        if(request.session.locale === 'flag-icon-pt') {
            languageParam = require('../locale/pt')
        }
        else {
            languageParam = require('../locale/en')
        }

        var client = new Client();
        var args = {
            data: { 
                email: request.payload.email,
                password: request.payload.password,
                livemode: "false",
                ipaddress : "0.0.0.0"
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };

        client.post(config.API.basedevurl + "/api/v1/customer/signin", args, function (data, response) {
            if(data['statusCode'] == 200) {
                
                emailInUse = request.payload.email;

                if(data['twowayauth']) {
                    randonTwoWaysURL = randtoken.generate(20, config.utils.randongenerate);
                    var data = {
                        statusCode : 200,
                        error: null,
                        message: 'twowayauth/' + randonTwoWaysURL 
                    };
                    return reply(data)
                }
                else {
                    request.session.logged = true;
                    request.session.sessiontoken = data['sessiontoken'];
               
                    var data = {
                        statusCode : 200,
                        error: null,
                        message: 'dashboard'
                    };
                    return reply(data)
                }
        
            }
            else if(data['statusCode'] == 403) {
                emailInUse = null;
                randonTwoWaysURL = null;

                var data = {
                    statusCode : 403,
                    error: 'Bad Request',
                    message: languageParam.loginWrongEmailPassword
                };
                
                return reply(data);
            }
            else {
                emailInUse = null;
                randonTwoWaysURL = null;

                var data = {
                    statusCode : 500,
                    error: 'Internal Error',
                    message: languageParam.internalError
                };
                
                return reply(data);
            }   
        });
    },

    ReLogin: function(request, reply) {
        var client = new Client();
        var args = {
            data: { 
                sessiontoken: request.session.sessiontoken,
                password: request.payload.password,
                livemode: "false",
                ipaddress : "0.0.0.0"
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };

        client.post(config.API.baseurl + "/api/v1/customer/resignin", args, function (data, response) {
            if(data['statusCode'] == 200) {
           
                var data = {
                    statusCode : 200,
                    error: 'null',
                    message: 'dashboard'
                };
                return reply(data)
            }
            else if(data['statusCode'] == 403) {
                var data = {
                    statusCode : 403,
                    error: 'Request Error',
                    message: 'Email ou Senha errada, verifique seus dados e tente novamente'
                };
                
                return reply(data);
            }
            else {
                var data = {
                    statusCode : 500,
                    error: 'Internal Error',
                    message: 'Ocorreu um erro interno por favor tente mais tarde'
                };
                
                return reply(data);
            }   
        });
    },

    SignOut: function(request, reply) {
        var languageParam = null;

        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        request.session.sessiontoken = null;
        request.session.logged = false;
        return reply.redirect('signin', info, { layout: 'authLayout' });
    },

    RenderTwoWaysAuthPage: function(request, reply) {
        request.session.logged = false;
        if(randonTwoWaysURL === null) {
            
            emailInUse = null;
            randonTwoWaysURL = null;

            return reply.view('signin', {}, { layout: 'authLayout' });   
        }
        else {
            
            randonTwoWaysURL = null;
            return reply.view('twowaysauth', {}, { layout: 'authLayout' });
        }
    },

    ValidateTwoWaysToken: function(request, reply) {
        ValidateTwoWaysTokenRequest(emailInUse, request.payload.token)
        .then(SuccessResponse => {
            emailInUse = null;
            
            request.session.logged = true;
            request.session.sessiontoken = SuccessResponse['sessiontoken'];
       
            var data = {
                statusCode : 200,
                error: null,
                message: '/dashboard'
            };
            return reply(data)
            
        })  
        .catch(error => {
            console.log(error);
            return reply(error);
        })  
    }
}

module.exports = login;