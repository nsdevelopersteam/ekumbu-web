var Client                      = require('node-rest-client').Client;
var config                      = require('../config');

var signup = {

    SignUpPagge : function(request, reply) {
        request.session.signupscc = false;
        request.session.activate = false;
        
        var languageParam = null;
        
        request.session.page = "signup"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        return reply.view('signup', info, { layout: 'authLayout' });
    },

    SignUpSuccessPagge : function(request, reply) {
        if(!request.session.signupscc) {
            request.session.signupscc = false;
            return reply.view('index', {}, { layout: 'default' });
        }
        request.session.signupscc = false;
        request.session.activate = true;
        return reply.view('signupsc', {}, { layout: 'authLayout' });
    },

    ActivatePage : function(request, reply) {

        //TODO Test closing browser
        if(!request.session.activate) {
            request.session.activate = false;
                // send to 500 page
          //  return reply.view('index', {}, { layout: 'default' });
        }

        // check email param
        var client = new Client();
        var args = {
            data: { 
                email: request.query.email,
                livemode: "false",
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };
        client.post(config.API.baseurl + "/api/v1/customer/chexeml", args, function (data, response) {
            
            if(data['result']) {
                // issue card
                var args = {
                    data: { 
                        email: request.query.email,
                        livemode: "false",
                    },
                    headers: { 
                        "Content-Type": "application/json",
                        "Authorization" : config.API.key
                    }
                };

                client.post(config.API.baseurl + "/api/v1/vendor/issuecard", args, function (data, response) {
                    if(data['statusCode'] == 200) {
                        request.session.activate = false;
                        var data = {
                            actmessage: 'Conta activada sucesso, já pode acessar e gerir sua conta ekumbu'
                        };
                        return reply.view('activate', data, { layout: 'authLayout' });
                    }
                    else {
                        var data = {
                            actmessage: 'Ocorreu um erro interno por favor tente mais tarde'
                        };
                        return reply.view('activate', data, { layout: 'authLayout' });
                    }
                    
                });
            }
            else {
                var data = {
                    actmessage: 'Erro, por favor clique no link que foi enviado no seu email'
                };
                return reply.view('activate', data, { layout: 'authLayout' });
            } 
        });
    },

    SignUp: function(request, reply) {

        var languageParam = null;
        if(request.session.locale === 'flag-icon-pt') {
            languageParam = require('../locale/pt')
        }
        else {
            languageParam = require('../locale/en')
        }

        var client = new Client();

        //encrypt email
        var args = {
            data: { 
                firstname: request.payload.fname,
                lastname: request.payload.lname,
                phone: request.payload.phone,
                email: request.payload.email,
                password: request.payload.password,
                redirecturl : config.server.baseurl + '/actaccount' + '?email=' + request.payload.email,
                livemode: config.API.livemode,
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };

        client.post(config.API.basedevurl + "/api/v1/vendor/createcustomer", args, function (data, response) {
            if(data['statusCode'] == 200) {
                request.session.signupscc = true; 
                var data = {
                    statusCode : 200,
                    error: null,
                    message: "signupsc"
                };
                return reply(data);
            }
            else {
                var data = {
                    statusCode : 500,
                    error: null,
                    message: languageParam.internalError
                };
                
                return reply(data);
            }
            
        });
    },

    CheckExistingEmail: function(request, reply) {

        var languageParam = null;
        if(request.session.locale === 'flag-icon-pt') {
            languageParam = require('../locale/pt')
        }
        else {
            languageParam = require('../locale/en')
        }

        var client = new Client();
        var args = {
            data: { 
                email: request.payload.email,
                livemode: "false",
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };
        client.post(config.API.baseurl + "/api/v1/customer/chexeml", args, function (data, response) {
            var dataR = {
                statusCode: 200,
                error: null,
                message: languageParam.emailexists,
                result: data['result']
            }
            reply(dataR); 
        });
    }
}

module.exports = signup;