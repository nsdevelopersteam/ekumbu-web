var index = {

    RenderDefault : function(request, reply) {
        var languageParam = null;

        request.session.page = "/"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
            selectedHome: 'current-menu-item current_page_item'
        }
        return reply.view('index', info, { layout: 'default' });
    },

    ChangeLanguage: function(request, reply) {
        
        if(request.payload.lang === 'pt') {
            request.session.locale = 'flag-icon-pt'
        }
        else {
            request.session.locale = 'flag-icon-us'
        }
        
        return reply({
            statusCode: 200,
            error: null,
            message: 'success',
            destination: request.session.page
        })
    }
}

module.exports = index;