
var developers = {

    RenderDefault : function(request, reply) {
        var languageParam = null;
        
        request.session.page = "developers"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
            selectedDevelopers : 'current-menu-item current_page_item'
        }
        return reply.view('developers', info, { layout: 'default' });
    }
}

module.exports = developers;