var Client                      = require('node-rest-client').Client;
var config                      = require('../config');
var Q                           = require('q');

var client = new Client();

// Send To Bank Account Attrb
var transfBankNane;
var transfCountryName;
var transfCountryID = -1;
var RealCountryID;
var bankNameChosen = false;
var submitTransferPassed = false;
var transferSuccessed = false;
var transfRate = 0;
var transfFee = 0;
var CurrencyToTransfer;
var TransferTrackingID;

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function findById(source, id) {
    return source.filter(function( obj ) {
        // coerce both obj.id and id to numbers 
        // for val & type comparison
        return +obj.index === +id;
    })[ 0 ];
}

function GetRateFromKwanzaRates(from) {
    var deferred = Q.defer();
    var args = {
        parameters: {
            from: from,
        },
        headers: { 
            "Content-Type": "application/json",
        }
    };
    client.get(config.rates.API, args, function (Rates, response) {
        if(Rates['statusCode'] == 200) {
            deferred.resolve(Rates);
        }
        else {
            deferred.reject(Rates['message']);
        }
    })
    
    return deferred.promise;
}

function RequestSendTransfer(sessiontokenParam, amountWFeeParam, originalAmountParam, amountToTransferParam, accountNumberParam, receiverNameParam, descriptionParam, ipaddressParam) {
    var deferred = Q.defer();
    var args = {
        data: { 
            sessiontoken : sessiontokenParam,
            amountWFee: amountWFeeParam,
            originalAmount: originalAmountParam,
            amountToTransfer : amountToTransferParam,
            fee: transfFee,
            rate: transfRate,
            accountNumber : accountNumberParam,
            receiverName : receiverNameParam,
            country: transfCountryName,
            currency: CurrencyToTransfer,
            institutionName: transfBankNane,
            livemode: config.API.livemode,
            description : descriptionParam,
            ipaddress : ipaddressParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };

    client.post(config.API.baseurl + "/api/v1/customer/sendTobank", args, function (TransferResponse, response) {
        if(TransferResponse['statusCode'] == 200) {
            deferred.resolve(TransferResponse);
        }
        else {
            deferred.reject(TransferResponse);
        }
    })
    
    return deferred.promise;
}

var transfer = {

    InitTransferRenderPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        var data = {
            countries : config.countriesToSend
        } 
        return reply.view('transferinit', data, { layout: 'generalDashboardOptLayout' });
    },

    ChooseCountryForTransfer: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        transfCountryID = request.payload.ccode;
        RealCountryID = transfCountryID;
        transfCountryName = findById(config.countriesToSend, transfCountryID).name
       
        var data = {
            statusCode: 200,
            error: null,
            message: 'transferchbank'
        }

        return reply(data);
    },

    ChooseBankForTransferRenderPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(transfCountryID === -1) {
            var dataC = {
                countries : config.countriesToSend
            } 
            return reply.view('transferinit', dataC, { layout: 'generalDashboardOptLayout' });
        }
        else {
            bankNameChosen = false;
            var dataReply = {
                banks : config.BanksByCountry[transfCountryID].banks
            }
            transfCountryID = -1;
            return reply.view('transferchbank', dataReply, { layout: 'generalDashboardOptLayout' }); 
        }
    },

    ChooseBankNameForTransfer: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(bankNameChosen === true) {
            var dataC = {
                countries : config.countriesToSend
            } 
            return reply.view('transferinit', dataC, { layout: 'generalDashboardOptLayout' });
        }
        else {
            transfBankNane = request.payload.bankName;
            bankNameChosen = true;
            submitTransferPassed = false;
            var data = {
                statusCode: 200,
                error: null,
                message: 'subtrasnfrdy'
            }
            return reply(data);
        }
    },

    SubmitTransferRenderPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(submitTransferPassed == true) {
            var dataC = {
                countries : config.countriesToSend
            } 
            return reply.view('transferinit', dataC, { layout: 'generalDashboardOptLayout' });
        }
        else {
            transfFee = findById(config.countriesToSend, RealCountryID).fee;
            CurrencyToTransfer = findById(config.countriesToSend, RealCountryID).currency;
            GetRateFromKwanzaRates(CurrencyToTransfer)
            .then(FoundRate => {
                transfRate = FoundRate.rate
                var dataReady = {
                    country: transfCountryName,
                    bank: transfBankNane,
                    transfRate: transfRate,
                    transfFee: transfFee,
                    currency: CurrencyToTransfer
                }
                submitTransferPassed = true;
                return reply.view('transfersubmit', dataReady, { layout: 'generalDashboardOptLayout' });
            })
            .catch(err => {
                return reply.view(new Error('500'));
            })
        }
    },

    SubmitTransferAction: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(submitTransferPassed == true) {
            transferSuccessed = true
            submitTransferPassed = false;
            RequestSendTransfer(request.session.sessiontoken, request.payload.amountWFee, request.payload.originalAmount, request.payload.amountToTransfer, request.payload.accountNumber, request.payload.receiverName, request.payload.description, request.info.remoteAddress)
            .then(TransferDone => {
                TransferTrackingID = TransferDone['trackingID']
                var rspInfo = {
                    statusCode: 200,
                    error: null,
                    message: "transfsuccess"
                }
                return reply(rspInfo)
            }).catch(err => {
                console.log(err)
                var rspInfo = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: err['message']
                }
                return reply(rspInfo)
            })
        }
        else {
            var dataC = {
                countries : config.countriesToSend
            } 
            return reply.view('transferinit', dataC, { layout: 'generalDashboardOptLayout' });
        }
    },

    RenderTransferSuccess: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(transferSuccessed == true) {
            transferSuccessed = false;
            var dataC = {
                trackingID: TransferTrackingID
            }
            return reply.view('transfsuccess', dataC, { layout: 'generalDashboardOptLayout' });
        }
        else {
            var dataC = {
                countries : config.countriesToSend
            } 
            return reply.view('transferinit', dataC, { layout: 'generalDashboardOptLayout' });
        }
    }
}

module.exports = transfer;