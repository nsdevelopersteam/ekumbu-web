var Client                      = require('node-rest-client').Client;
var config                      = require('../config');
var Q                           = require('q');

var client = new Client();
var Typechosen = false;

//Deposit Fields 
var amountInserted;
var amountToTransfer;
var depositDescription;
var statementDescription;
var cardNumber;
var cardExp;
var cardCVV;
var depositCurrency
var totalWithTax;
var amountToAddToEkumbu;
var dpRate;
var dpFee;

var DeposittrackingID;
//end

var activateFinishDeposit = false;
var activateDepositDone = false;

var FoundRate;

//Send Money Attrb
var sendSuccesseded = false;
var tranferTrackingID;

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function MakeDeposit(totalWTax, ip, sessionToken) {
    var deferred = Q.defer();

    var month = cardExp.substr(0, 2);   
    var year;
    if(cardExp.lenght == 7) {
        year = cardExp.substr(3, 8)
    }
    else {
        year = cardExp.substr(3, 6)
    }

    var args = {
        data: { 
            cardNumber : cardNumber,
            expmonth : month,
            expyear : year,
            cvv : cardCVV,
            sessiontoken : sessionToken,
            amount : totalWTax,
            amouttransfer : amountToTransfer,
            livemode: config.API.livemode,
            ipaddress : ip,
            description : depositDescription,
            statementdescription : statementDescription,
            currency : depositCurrency,
            amountToEkb : amountToAddToEkumbu,
            rate : dpRate,
            fee: dpFee.toString()
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.deposits.access_key
        }
    };

    client.post(config.deposits.url + "/dfcard", args, function (DepositResponse, response) {
        if(DepositResponse['statusCode'] == 200) {
            deferred.resolve(DepositResponse);
        }
        else {
            deferred.reject(DepositResponse['message']);
        }
    })
    
    return deferred.promise;
}

function MakeDepositFromTopUpCard(cardNumberParam, sessionTokenParam, descriptionParam, ipaddressParam) {
    var deferred = Q.defer();

    var args = {
        data: { 
            cardnumber: cardNumberParam,
            sessiontoken: sessionTokenParam,
            description: descriptionParam,
            ipaddress: ipaddressParam
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };

    client.patch(config.API.baseurl + "/api/v1/manager/valrechargeablecard", args, function (DepositResponse, response) {
        console.log(DepositResponse)
        if(DepositResponse['statusCode'] == 200) {
            deferred.resolve(DepositResponse);
        }
        else {
            deferred.reject(DepositResponse);
        }
    })
    
    return deferred.promise;
}

function AuthDocumentToView(documentID, sessionID) {
    var deferred = Q.defer();
    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : "Bearer " + sessionID
        }
    };
    client.get(config.documents.url + "/documents/" + documentID, args, function (Doc, response) {
        deferred.resolve(Doc)
    })
    
    return deferred.promise;
}

function GetRateFromKwanzaRates(from) {
    var deferred = Q.defer();
    var args = {
        parameters: {
            from: from,
        },
        headers: { 
            "Content-Type": "application/json",
        }
    };
    client.get(config.rates.API, args, function (Rates, response) {
        if(Rates['statusCode'] == 200) {
            deferred.resolve(Rates);
        }
        else {
            deferred.reject(Rates['message']);
        }
    })
    
    return deferred.promise;
}

function GetBankAccount(sessionToken) {

    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.baseurl + "/api/v1/customer/getctsource", args, function (dataBankAccountInfo, response) {
        if(dataBankAccountInfo['statusCode'] == 200) {
            deferred.resolve(dataBankAccountInfo);
        }
        else if(dataBankAccountInfo['statusCode'] == 403) {
            if(dataBankAccountInfo['message'] == "There is not account linked")  {
                deferred.resolve(dataBankAccountInfo);
            }
            else {
                deferred.reject(dataBankAccountInfo['message']);
            }
        }
        else {
            deferred.reject(dataBankAccountInfo['message']);
        }
    })

    return deferred.promise;
}

function GetEventDetail(evtID, sessiontokenP) {
    
    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessiontokenP,
            livemode: config.API.livemode,
            srcID: evtID
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.baseurl + "/api/v1/customer/gevtdetail", args, function (Detail, response) {
        if(Detail['statusCode'] == 200) {
            deferred.resolve(Detail);
        }
        else {
            deferred.reject(Detail['message']);
        }
    })

    return deferred.promise;
}

var customerOpt = {

    RenderAddBank : function(request, reply) {

        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        var client = new Client();
        var args = {
            parameters: { 
                sessiontoken: request.session.sessiontoken,
                livemode: config.API.livemode
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };
        client.get(config.API.baseurl + "/api/v1/vendor/gtemisab", args, function (data, response) {
            var dataR = {
                banks: data['banks']
            }

            return reply.view('addbank', data, { layout: 'generalDashboardOptLayout' });
        });
    },

    RenderViewBankAccount: function(request, reply){
        GetBankAccount(request.session.sessiontoken)
        .then(BankInfo => {
            data = {
                balogo : BankInfo['banklogo'],
                banumber : BankInfo['accountnumber'],
                baname : BankInfo['bankname']
            }
            return reply.view('viewbank', data, { layout: 'generalDashboardOptLayout' })
        })
        .catch(err=> {

        })
    },

    AddCustomerBankAccount: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }
        
        var client = new Client();
        var args = {
            data: { 
                sessiontoken: request.session.sessiontoken,
                livemode: config.API.livemode,
                bankname : request.payload.bankname,
                accountnumber : request.payload.accnumber,
                iban : request.payload.acciban
            },
            headers: { 
                "Content-Type": "application/json",
                "Authorization" : config.API.key
            }
        };
        client.patch(config.API.baseurl + "/api/v1/vendor/addbaccount", args, function (data, response) {

            if(data['statusCode'] == 200) {
                var dataR = {
                    statusC : 200,
                    error: 'null',
                    message : 'success'
                }
                reply(dataR);
            }
            else if(data['statusCode'] == 403) {
                if(data['message'] == 'Wrong account number or iban') {
                    var dataR = {
                        statusC : 403,
                        error: 'Request Error',
                        message : 'Número de conta or iban incorrectos. Por favor verifque seus dados'
                    }
                    reply(dataR);
                }
                
            }
            else {
                var dataR = {
                    statusC : 500,
                    error: 'Internal Error',
                    message : 'Occoreu um erro interno, por favor tente mais tarde'
                }
                reply(dataR);
            }
        });
    },

    RenderDepositPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        if(Typechosen == false) {
            return reply.view('chdeposit', {}, { layout: 'generalDashboardOptLayout' });
        }
        return reply.view('deposit', {}, { layout: 'generalDashboardOptLayout' });
    },

    RenderChooseDepositType : function(request, reply) {
        var languageParam = null;
        
        request.session.page = "choosedpt"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.redirect('signin', info, { layout: 'authLayout' });
        }
        else {
            Typechosen = false;
            return reply.view('chdeposit', info, { layout: 'generalDashboardOptLayout' });
        }
    },

    ChooseDepositType : function(request, reply) {
        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', {}, { layout: 'authLayout' });
        }

        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        if(request.payload.type == 'cc') {
            jsonResponse = {
                statusCode : 200,
                error : null,
                message : 'depositcc'
            }
            Typechosen = true;
            return reply(jsonResponse);
        }
        else if(request.payload.type == 'etopup') {
            jsonResponse = {
                statusCode : 200,
                error : null,
                message : 'etopupdepsit'
            }
            Typechosen = true;
            return reply(jsonResponse);
        }
        else {
            jsonResponse = {
                statusCode : 403,
                error : 'Invalid Request',
                message : languageParam.choosedepositType
            }
            return reply(jsonResponse);
        }

    },

    GetRate : function(request, reply) {
        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.redirect('signin', info, { layout: 'authLayout' });
        }

        GetRateFromKwanzaRates(request.query.from)
        .then(RatesFound => {
            FoundRate = RatesFound['rate']
            return reply(RatesFound);
        }) 
        .catch(err => {
            console.log(err)
            /*TODO Send to internal error page */
        })
    },

    GetDepositReady: function(request, reply) {
        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }

        activateFinishDeposit = true;
        amountInserted = request.payload.amt;
        amountToTransfer = request.payload.amounttoget;
        depositDescription = request.payload.dpdesc;
        statementDescription = request.payload.stdesc;
        cardNumber = request.payload.cardn;
        cardExp = request.payload.cardexp;
        cardCVV = request.payload.cardcvv;
        depositCurrency = request.payload.currency;
        amountToAddToEkumbu = request.payload.amtoeku;

        data = {
            statusCode : 200,
            error: null,
            message : 'fndeposit'
        }
        return reply(data);

    },

    RenderFinishDeposit: function(request, reply) {
        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }

        if(!activateFinishDeposit) {
            Typechosen = false;
            return reply.view('chdeposit', info, { layout: 'generalDashboardOptLayout' });
        }

        var calculatedFee = (amountInserted*config.deposits.fee)/100;
        var totalToPay = Number(calculatedFee)+Number(amountInserted);
        totalWithTax = totalToPay;
        dpRate = FoundRate;
        dpFee = calculatedFee;
        activateFinishDeposit = false;

        data = {
            feePercent : config.deposits.fee,
            feeValue : calculatedFee,
            toreceive : amountToTransfer,
            rate : FoundRate,
            toPaywtotT : amountInserted,
            currency : depositCurrency,
            totaltopay : totalToPay,
            language: languageParam,
            locale : request.session.locale,
        }
        return reply.view('finishdeposit', data, { layout: 'generalDashboardOptLayout' });
    },

    SendDeposit: function(request, reply) {
        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        MakeDeposit(totalWithTax, request.info.remoteAddress, request.session.sessiontoken)
        .then(successfulDeposit => {
            activateDepositDone = true;
            jsonResponse = {
                statusCode: 200,
                error : 'null',
                message: 'scdeposit',
            }
            DeposittrackingID = successfulDeposit['trackingID']
            return reply(jsonResponse)
        }) 
        .catch(err => {
            console.log(err)
            if(err === "Customer Invalid Card or Card Expired" || err === "Invalid Card or Insuficient Funds") {
                jsonResponse = {
                    statusCode: 403,
                    error : 'Request Error',
                    message:  "Não há fundos suficientes no cartão para completar o deposito"
                }
                return reply(jsonResponse)    
            }
            else if (err === "Internal Error Creating Deposit, but the charge was made" || err === "Internal Error Creating Token, but the charge was made")  {
                jsonResponse = {
                    statusCode: 403,
                    error : 'Request Error',
                    message: "Seus fundos foram transferidos mas ocorreu um erro interno na finalização do deposito, entraremos em contacto consigo para finalizar o deposito. Por favor não efectuar um novo deposito, sem antes entrarmos em comunicação"
                }
                return reply(jsonResponse)
            }
            else {
                jsonResponse = {
                    statusCode: 403,
                    error : 'Request Error',
                    message:  "Ocorreu um erro interno por favor tente mais tarde"
                }
                return reply(jsonResponse)    
            }
            
        })
    },

    RenderSuccessDeposit: function(request, reply) {

        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        activateFinishDeposit = false;
        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }
        if(!activateDepositDone) {
            return reply.view('chdeposit', info, { layout: 'generalDashboardOptLayout' });
        }
        data = {
            language: languageParam,
            locale : request.session.locale,
            trackingID : DeposittrackingID
        }
        return reply.view('scdeposit', data, { layout: 'generalDashboardOptLayout' });
    },

    GetEventDetails : function(request, reply) { 
        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }
        else {
            GetEventDetail(request.query.did, request.session.sessiontoken)
            .then(FoundDetail => {
                var docUrl='none';

                if(FoundDetail.attachment) {
                    if(FoundDetail.attachment.fileID != 'none') {
                        docUrl = config.documents.url + "/documents/" + FoundDetail.attachment.fileID + "/" + request.session.sessiontoken;
                    }
                }
                var response = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    details : FoundDetail,
                    docurl: docUrl 
                }
                return reply(response)
            })
            .catch(err=> {
                console.log(err)
                var response = {
                    statusCode: 404,
                    error: 'not found',
                    message: 'Ocorreu um erro interno por favor tente mais tarde'
                }
                return reply(response)
            })
        }
    },

    RenderEtopUpDepositPage: function(request, reply) {
        var languageParam = null;
        
        request.session.page = "etopupdepsit"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.redirect('signin', info, { layout: 'authLayout' });
        }

        if(Typechosen == false) {
            return reply.redirect('chdeposit', info, { layout: 'generalDashboardOptLayout' });
        }
        else {
            request.session.dpstopup = undefined;
            return reply.view('etopupdeposit', info, { layout: 'generalDashboardOptLayout' });
        }
    },

    DepositFromEtopUpCard: function(request, reply) {

        var languageParam = null;
        
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }

        MakeDepositFromTopUpCard(request.payload.cardn, request.session.sessiontoken, request.payload.description, request.info.remoteAddress)
        .then(DepositResponse => {
            request.session.dpstopup = DepositResponse.trackingID;
            
            return reply({
                statusCode: 200,
                error: null,
                message: 'etopupsucess'
            })
        })
        .catch(error => {
            console.log(error)
            return reply(error)
        })
    },

    RenderSuccessEtopUpDepositPage: function(request, reply) {
        var languageParam = null;
        
        request.session.page = "etopupsucess"
        if(request.session.locale === undefined) {
            request.session.locale = 'flag-icon-pt'
            languageParam = require('../locale/pt')
        }
        else {
            if(request.session.locale === 'flag-icon-pt') {
                //load pt locale
                languageParam = require('../locale/pt')
                request.session.locale = 'flag-icon-pt'
            }
            else {
                languageParam = require('../locale/en')
                request.session.locale = 'flag-icon-us'
                //load english locale
            }
        }

        var info = {
            language: languageParam,
            locale : request.session.locale,
        }

        if(request.session.logged === false || request.session.logged === undefined) {
            return reply.view('signin', info, { layout: 'authLayout' });
        }

        if(request.session.dpstopup === undefined) {
            return reply.view('chdeposit', info, { layout: 'generalDashboardOptLayout' });
        }
        else {
            data = {
                trackingID : request.session.dpstopup,
                language: languageParam,
                locale : request.session.locale,
            }
            request.session.dpstopup = undefined;
            return reply.view('scdepositftopup', data, { layout: 'generalDashboardOptLayout' });
        }
    },
}

module.exports = customerOpt;