var Client                      = require('node-rest-client').Client;
var config                      = require('../config');
const moment                    = require('moment')
var Q                           = require('q');
var client = new Client();

var searchStartDate = moment().subtract(30, 'days').format("MM-DD-YYYY");
var searchEndDate = moment().format("MM-DD-YYYY");
var searchOption = "all";
var smsconfirmation = 'checked';
var blocked = "checked";

var loadlimitParam = 6;

function GetCardInfo(sessionToken) {

    var deferred = Q.defer();
    var args = {
        data: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.post(config.API.basedevurl + "/api/v1/customer/getccardinf", args, function (dataBasicInfo, response) {
        if(dataBasicInfo['statusCode'] == 200) {
            deferred.resolve(dataBasicInfo);
        }
        else {
            deferred.reject(dataBasicInfo['message']);
        }
    })
    
    return deferred.promise;
}

function GetCustomerBalance(sessionToken) {

    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.basedevurl + "/api/v1/customer/getbalance", args, function (AccountBalance, response) {
        if(AccountBalance['statusCode'] == 200) {
            deferred.resolve(AccountBalance);
        }
        else {
            deferred.reject(AccountBalance['message']);
        }
    })

    return deferred.promise;
}

function GetCustomerEvents(sessionToken) {
    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.baseurl + "/api/v1/customer/cgetallevt", args, function (dataCustEvents, response) {
        if(dataCustEvents['statusCode'] == 200) {
            deferred.resolve(dataCustEvents);
        }
        else {
            deferred.reject(dataCustEvents['message']);
        }
    })
    
    return deferred.promise;
}

function GetCustomerEventsNoLimit(sessionToken, fromParam, toParam) {
    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
            from: fromParam,
            loadlimit: loadlimitParam,
            to: toParam
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.baseurl + "/api/v1/customer/cgetallevtnolimit", args, function (dataCustEvents, response) {
        if(dataCustEvents['statusCode'] == 200) {
            deferred.resolve(dataCustEvents);
        }
        else {
            deferred.reject(dataCustEvents['message']);
        }
    })
    
    return deferred.promise;
}

function GetCustomerEventsAllByType(sessionToken, typeParam, fromParam, toParam) {
    var deferred = Q.defer();
    var args = {
        parameters: {
            sessiontoken: sessionToken,
            livemode: config.API.livemode,
            from : fromParam,
            to: toParam,
            type: typeParam
        },
        headers: { 
            "Authorization" : config.API.key,
            "Content-Type": "application/json",
        }
    };
    client.get(config.API.baseurl + "/api/v1/customer/cgetallevttype", args, function (dataCustEvents, response) {
        if(dataCustEvents['statusCode'] == 200) {
            deferred.resolve(dataCustEvents);
        }
        else {
            deferred.reject(dataCustEvents['message']);
        }
    })
    
    return deferred.promise;
}

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function ChangeCardSMSConfirmation(sessionToken, statusParam) {
    var deferred = Q.defer();

    var args = {
        data: { 
            sessiontoken : sessionToken,
            status: statusParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };
    client.patch(config.API.baseurl + "/api/v1/customer/changesmsconfirmation", args, function (ResponseData, response) {
        if(ResponseData['statusCode'] == 200) {
            deferred.resolve(ResponseData);
        }
        else {
            deferred.reject(ResponseData);
        }
    })
    
    return deferred.promise;
}

function ChangeBlockStatusRequest(sessionToken, statusParam) {
    var deferred = Q.defer();

    var args = {
        data: { 
            sessiontoken : sessionToken,
            status: statusParam,
        },
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.key
        }
    };
    client.patch(config.API.baseurl + "/api/v1/customer/changecardblockstatus", args, function (ResponseData, response) {
        if(ResponseData['statusCode'] == 200) {
            deferred.resolve(ResponseData);
        }
        else {
            deferred.reject(ResponseData);
        }
    })
    
    return deferred.promise;
}

var dashboard = {

    DashBoardPage : function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session)) {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }
        
        GetCardInfo(request.session.sessiontoken)
        .then(CustomerInfo => {
            GetCustomerBalance(request.session.sessiontoken)
            .then(AccountBalance => {
                GetCustomerEvents(request.session.sessiontoken) 
                .then(CustomerEvents => {
                    var hasTrans;
                    var CustoTransac;
                    smsconfirmation = 'checked'
                    blocked = "checked"

                    if(CustomerEvents['nrecords'] == 0) {
                        hasTrans = false;
                    }
                    else {
                        hasTrans = true;
                        CustoTransac = CustomerEvents['events']
                    }
                    if(!CustomerInfo['smsconfirmation']) {
                        smsconfirmation = "none";
                    }

                    if(!CustomerInfo['blocked']) {
                        blocked = "none";
                    }

                    var languageParam = null;
                    
                    request.session.page = "dashboard"
                    if(request.session.locale === undefined) {
                        request.session.locale = 'flag-icon-pt'
                        languageParam = require('../locale/pt')
                    }
                    else {
                        if(request.session.locale === 'flag-icon-pt') {
                            //load pt locale
                            languageParam = require('../locale/pt')
                            request.session.locale = 'flag-icon-pt'
                        }
                        else {
                            languageParam = require('../locale/en')
                            request.session.locale = 'flag-icon-us'
                            //load english locale
                        }
                    }

                    dataR = {
                        transactions : CustoTransac,
                        cardinfo : {
                            cardn : CustomerInfo['cardnumber'],
                            cardexpd: CustomerInfo['cardexpdate'],
                            cardcn : CustomerInfo['cardlivetoken'],
                        },
                        balance : AccountBalance['balance'],
                        hasTransactions : hasTrans,
                        username : CustomerInfo['cardholdername'],
                        smsconf: smsconfirmation,
                        blocked: blocked,
                        language: languageParam,
                        locale : request.session.locale,
                    }

                    return reply.view('dashboard', dataR, { layout: 'dashboardLayout' });
                })
                .catch(err => {
                    console.log("error from get customer event: " + err);
                    request.session.logged = false;
                    return reply.view(new Error('500'));
                })
            })
            .catch(err => {
                console.log("error from get customer balance: " + err);
                request.session.logged = false;
                return reply.view(new Error('500'));
            })
        })
        .catch(err => {
            console.log("error from get card info: " + err);
            request.session.logged = false;
            return reply.view(new Error('500'));
        })
    },

    RenderAllEventsPage: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session) || request.session == '') {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }

        if(searchOption === "all") {
            GetCustomerEventsNoLimit(request.session.sessiontoken, searchStartDate, searchEndDate) 
            .then(CustomerEvents => {
                var hasTrans;
                var CustoTransac;

                if(CustomerEvents['nrecords'] == 0) {
                    hasTrans = false;
                }
                else {
                    hasTrans = true;
                    CustoTransac = CustomerEvents['events']
                }

                dataR = {
                    transactions : CustoTransac,
                    hasTransactions : hasTrans,
                    option : searchOption,
                    startDate : searchStartDate,
                    endDate: searchEndDate
                }

                return reply.view('allevents', dataR, { layout: 'generalDashboardOptLayout' });
            })
            .catch(err => {
                console.log(err);
                request.session.logged = false;
                return reply.view(new Error('500'));
            })
        }
        else {
            GetCustomerEventsAllByType(request.session.sessiontoken, searchOption, searchStartDate, searchEndDate)
            .then(CustomerEvents => {
                var hasTrans;
                var CustoTransac;

                if(CustomerEvents['nrecords'] == 0) {
                    hasTrans = false;
                }
                else {
                    hasTrans = true;
                    CustoTransac = CustomerEvents['events']
                }

                dataR = {
                    transactions : CustoTransac,
                    hasTransactions : hasTrans,
                    option : searchOption,
                    startDate : searchStartDate,
                    endDate: searchEndDate
                }

                return reply.view('allevents', dataR, { layout: 'generalDashboardOptLayout' });
            })
            .catch(err => {
                console.log(err);
                request.session.logged = false;
                return reply.view(new Error('500'));
            })   
        }
    },

    LogOut : function(request, reply) {
        request.session = {};
        var data = {
            statusC : 200,
            error: null, 
            message: 'signin'
        };
        return reply(data);
    },

    ChangeSearchValues: function(request, reply) {
        if(request.session == null || request.session == undefined || isEmptyObject(request.session) || request.session == '') {
            return reply.redirect('signin', {}, { layout: 'authLayout' });
        }

        var from = request.query.dateRange.substring(0,10)
        var to = request.query.dateRange.substring(11,request.query.dateRange.lenght)

        searchStartDate = from;
        searchEndDate = to;
        searchOption = request.query.type;
        
        var infoResponse = {
            statusCode : 200,
            error: null,
            message: 'success'
        }
        return reply(infoResponse)
    },

    ChangeSMSConfirmation: function(request, reply) {
        var boolStatus = false;

        if(request.payload.status === 'none') {
            boolStatus = true;
        }
        else {
            boolStatus = false;
        }

        ChangeCardSMSConfirmation(request.session.sessiontoken, boolStatus)
        .then(responseInfo => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
            }
            return reply(infoResponse)
        })
        .catch(ErrorResponse => {
            //TODO format response
            var infoResponse = {
                statusCode: ErrorResponse.statusCode,
                error: ErrorResponse.error,
                message: ErrorResponse.message,
            }
            return reply(infoResponse)
        })
    },

    ChangeBlockStatus: function(request, reply) {
        var boolStatus = false;

        if(request.payload.status === 'none') {
            boolStatus = true;
        }
        else {
            boolStatus = false;
        }

        ChangeBlockStatusRequest(request.session.sessiontoken, boolStatus)
        .then(responseInfo => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
            }
            return reply(infoResponse)
        })
        .catch(ErrorResponse => {
            //TODO format response
            console.log(ErrorResponse)
            return reply(ErrorResponse)
        })
    }

}

module.exports = dashboard;