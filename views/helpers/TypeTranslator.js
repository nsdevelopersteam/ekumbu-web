const formatCurrency = require('format-currency')

var types = {
    "send" : "envio",
    "payment" : "pagamento",
    "deposit" : "depósito",
    "transfer" : "transferencia",
    "processing" : "processando",
    "completed" : "completa",
    "succeded" : "completo",
    "pending" : "pendente",
    "Processing" : "processando",
    "confirmed" : "confirmado"
}

module.exports = function(item) {

    var typeFormated = types[item]
    return typeFormated;

};