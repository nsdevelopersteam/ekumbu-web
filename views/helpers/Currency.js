const formatCurrency = require('format-currency')

module.exports = function(item) {

    let opts = { format: '%v %c', code: 'AOA' }
    var result = formatCurrency(item, opts);
    console.log(result)
    return result;

};