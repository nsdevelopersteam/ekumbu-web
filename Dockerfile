FROM node

WORKDIR /usr/src/app
RUN npm install && npm install -g nodemon

ADD . /usr/src/app

EXPOSE 3005

CMD ["npm", "start"]
CMD ["node", "server.js"]