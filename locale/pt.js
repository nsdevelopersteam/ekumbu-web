module.exports = {

    //general 
    "ekumbu": "ekumbu",
    "cancel": "Cancelar",

    //Error messages
    "internalError" : "Ocorreu um erro interno por favor tente mais tarde",
    "loginInvalidEmail": "Por favor insira um email válido",
    "loginInvalidPassword" : "Por favor insira uma senha válida",
    "loginWrongEmailPassword": "Email ou Senha errada, verifique seus dados e tente novamente",
    "emailexists" : "Já existe uma conta registrada com esse email, clique em Minha Conta para acessar",

    "signupInvalidEmail" : "Para facilitar a comunicação por favor digite um e-mail válido",
    "signupInvalidpassword": "Sua senha deve conter no minimo 6 digitos, 1 letra maiscula, 1 simbolo do tipo (!@#$%) e um número",
    "signupInvalidfirstname": "Digite o seu primeiro nome como aparece no documento de identificação",
    "signupInvalidflastname": "Digite o seu ultimo nome como aparece no documento de identificação",
    "signupInvalidphone" : "Digite o seu número de telefone que está ativo",
    "signupinvaliddata": "Preencha todos os dados e correctamente",
    "signuppasswordshoudbeequal": "as senhas devem ser iguais",
    "choosedepositType" : "Escolha o tipo de deposito",

    "errorTitle" : "Erro",

    //Index
    "callus" : "Liga-nos",

        //index top menus 
        "support" : "Suporte",
        "signup" : "Nova Conta",
        "signin" : "Minha Conta",

        //Main Menu
        "mainMenuhome" : "Home",
        "mainMenuservices" : "Serviços",
        "mainMenupayments" : "Pagamentos",
        "mainMenudeposits" : "Depósitos",
        "mainMenutransfers" : "Transfêrencias",
        "mainMenusend" : "Envios",
        "mainMenuprogrammers" : "Programadores",
        "mainMenudocumentation" : "Documentação",
        "mainMenuapistatus" : "Estado da API",
        "mainMenuapireference" : "Referência da API",
        "mainMenubegin" : "Começar",
        "mainMenumerchants" : "Comerciantes",
        "mainMenucontactus" : "Contacta-nos",

        //Slider
        "mainSlider1Title" : "Seguro & Efectivo",
        "mainSlider1Description": "Chegou a solução para Pagamentos Online",
        "mainSlider2Title": "Facilite sua vida com o cartão mágico",
        "mainSlider2DescriptionPart1": "Facilite sua vida com o cartão mágico",
        "mainSlider2DescriptionPart2" : "Com o ekumbu poderá fazer depositos e pagamentos rapidos.",
        "mainSlider2DescriptionPart3" : "Cartão ekumbu permite-lhe fazer depositos e pagamentos.",
        "mainSlider3Title": "Seguro",
        "mainSlider3DescriptionPart1": "Ekumbu protege seus dados usando tecnologia de ultima geração",
        "mainSlider3DescriptionPart2" : "A segurança no ekumbu tem prioridade maxima",
        "mainSlider3DescriptionPart3" : "use o cartão ekumbu com confiança",


        "slider1title" : "Facil de Usar",
        "slider1description" : "O cartão Ekumbu permite-lhe fazer depositos, pagamentos e transferencias de forma facil, não fique de fora adira ja o seu!",
        "slider2title" : "Ekumbu SMS",
        "slider2description" : "Ekumbu permite-lhe fazer operações sem ter acesso a internet. Efectue transferencias, bloqueie seu cartão e muito mais por SMS",
        "slider3title" : "Ekumbu Mobile",
        "slider3description" : "Usando a Ekumbu APP disponivel para Iphone e Android é possivel fazer todo tipo de transações com o seu cartão. Baixe já na AppleStore ou GoogleStore ",
        "slider4title" : "Segurança",
        "slider4description" : "Active autenticação em dois passos. Autenticação em dois passos so é possivel aceder a sua conta Ekumbu com um codigo de autorização",
        "sliderbuttonlearnmore" : "Saber Mais",

    "watchvideodescription" : "Assista uma demostração de como efectuar um deposito e pagamento.",
    "watchvideobutton" : "Ver Video",

    "companiesUsingEkumbu" : "Empresas e Lojas aceitando Ekumbu",

        //Botton
        "ekumbuDescription" : "Ekumbu é um cartão de pagamentos e permite a gestão do seu dinheiro de forma segura e simples.",
        "bottonUtilLinks": "Links Uteis",
        "bottonGetEkumbu": "Obter Ekumbu",
        "buttonHowToDeposit": "Como Depositar",
        "bottonHowToSecureEkumbu": "Garantir a segurança do Cartão",
        "bottonHowToPay": "Como efectuar Pagamentos",
        "bottonEkumbuMobile": "Ekumbu Mobile",
        "bottonEkumbuSMS": "Ekumbu SMS",
        "bottonEkumbuCompanies": "Empresas",
        "bottonwhatisEkumbuCompany": "O que é Ekumbu Empresas ?",
        "bottonDepositTogether": "Depositos conjuntos",
        "bottonWorkWithUs": "Trabalhe conosco",
        "bottonSolutions": "Soluções",
        "bottonContactForm": "Formulário de Contacto",
        "bottonContactFormQuestion": "Sua Questão",
        "bottonContactFormSendButton": "Enviar",

    //PROGRAMMERS
    "programmerTitle": "Programadores",
    "programmerSubTitle": "Aceite pagamentos online de forma rápida, simple e segura",
    "programmerSubTitle2Part1": "Integre pagamentos ",
    "programmerSubTitle2Part2": " no seu sistema usando a sua linguagem favorita com facilidade e receba assitencia quando precisar!",
    "programmersComunity": "Comunidade",
    "programmerComunityDescription": "Faça parte da comunidade e aceite pagamentos de forma simples",
    "programmersLinkPayments": "Pagamentos",
    "programmersLinkIntegration": "Integração",
    "programmersLinkDocumentation": "Documentação",
    "programmersLinkAPIReference": "Refêrencia da API",
    "programmersLinkGetStaterd": "Começar",
    "programmersLinkTests": "Testes",
    "programmersLinkAPILibraries": "Bibiliotecas da API",

    //LOGIN
    "loginTitle": "Acessar Conta",
    "loginPassword": "Senha",
    "loginButtonSignIn": "Entrar",
    "loginForgotPassword" : "Esquceu a Senha ?",


    //SIGNUP
    "signUpStope1Title": "Criar Conta",
    "signUpStope2Title": "Dados Pessoais",
    "signUpStope3Title": "Finalizar",

    "signUpstep1Title": "Nova Conta",
    "signUpstep1description": "Compre online usando o ekumbu, abrindo uma conta agora!",

    "signUpstep2Title": "Dados Pessoais",
    "signUpstep2Description": "Seus dados são mantidos completamente seguros!",

    "signUpstep3Title": "Finalizar",
    "signUpstep3SubTitle": "Obrigado!",
    "signUpStep3Description": "Obrigado por fazeres parte da familia Ekumbu, clique em Finalizar para concluir o processo",

        "signUpDataPassword": "Senha",
        "signUpDataConfirmPassword": "Confirmar Senha",
        "signUpButtonNext": "Seguinte",
        "signUpDatafirstName": "Primeiro Nome",
        "signUpDataLastName": "Ultimo Nome",
        "signUpDataPhone": "Telefone",
        "signUpFinalizeButton": "Finalizar",

    "signupsuccessTitle": "Feito !",
    "signupsuccessdescription": "Conta criada com sucesso, conclua o processo verificando seu email.",

    //DASHBOARD
    "dashboardHi" : "Ola, ",
    "dashboardMenuDeposit": "Depositar",
    "dashboardMenuTransfer": "Transferir",
    "dashboardMenuSend": "Enviar",
    "dashboardTopMenuActivities": "Actividades",
    "dashboardTopMenuSettings" : "Definições",
    "dashboardTopMenuExit" : "Sair",

    "activatedCard" : "Cartão Ekumbu Activo",
    "activatedCardnumber": "número",
    "activatedCardexpdate": "válido até",
    "activatedCardControlnumber": "n. controle",
    "activatedCardSecCode": "cod. secreto",
    "activatedCardShow": "mostrar",
    "dashboardAvailableAmount": "Montante Disponivel",
    "dashboardpaymentAuth": "Autorização de Pagamentos",
    "dashboardpaymentBlock": "Bloquear",
    "dashboardManagment": "Gestão",
    "dashboardManagmentMakDepos": "Efectuar Depositos",
    "dashboardManagSendMoney": "Enviar para outro Ekumbu",
    "dashboardModalAuthPayment": "Autorizar Pagamentos",
    "dashboardModalAuthorize": "Autorizar",
    "dashboardModalEnable": "Inativo",
    "dashboardModalDisable": "Activo",
    "dashboardModalBlockCard": "Bloquear o Cartão",
    "dashboardModalBlockDescription": "Bloquear",
    "dashboardModalAuthorizeQuestionEnabled": "Activando a confirmação por SMS, receberá uma SMS para autorizar cada pagamentos que for fazer.",
    "dashboardModalAuthorizeQuestionDisabled" : "Desativando a confirmação por SMS, seus pagamentos não precisarão de autorização por SMS. Não Recomendado!",
    "dashboardModalQuestion": "Tem a certeza?",
    "dashboardModalBlockQuestionEnabled": "Bloqueando o seu cartão, não consigará efectuar nenhum pagamento",
    "dashboardModalBlockQuestionDisabled": "Desbloqueando o seu cartão, poderá efectuar pagamentos com o mesmo",
    "dashboardModalYes": "Sim",
    "dashboardModalNo": "Não",

    //simpleTableView
    "simpleTableViewTitle" : "transações completas",
    "simpleTableViewNoRecord": "Não há transações feitas de momento.",
    "simpleTableViewSeeAll": "ver todas",
    "simpleTableViewModalTransactionDetails": "Detalhes da Transação",
    "simpleTableViewModalTotalTransfered": "Total Transferido",
    "simpleTableViewModalTotalTobeTransfered": "Total Transferido",
    "simpleTableViewModalTaxPaid": "Taxa Paga",
    "simpleTableViewModalTotalReceived" : "Total recebido",
    "simpleTableViewModalRate": "Cambio",
    "simpleTableViewModalDepositStatus": "Estado do Deposito",
    "simpleTableViewModalTrackingID": "Código de Rastreamento",
    "simpleTableViewModalStatus": "Estado",
    "simpleTableViewModalDepositSource": "Origem do deposito",
    "simpleTableViewModalDepositFrom": "Deposito efectuado apartir de",
    "simpleTableViewModaldate": "em",
    "simpleTableViewModalPayment": "Pagamento",
    "simpleTableViewModalPaymentFor": "Efectuado para",
    "simpleTableViewModalPaymentAmount": "Montante",
    "simpleTableViewModalTransferDetails": "Detalhes da Transferencia",
    "simpleTableViewModalDetails": "Detalhes",
    "simpleTableViewModalEkumbuCardUsed": "Cartão Ekumbu Usado",
    "simpleTableViewModalSendSource": "Origem do Envio",
    "simpleTableViewModalSentFrom": "Enviado a partir de",
    "simpleTableViewModalDescription": "com a descrição",
    "simpleTableViewModalAmountDestination" : "Destino do montante",
    "simpleTableViewModalSentTo": "Enviado para",
    "simpleTableViewModalReceiverBank" : "Instituição que recebeu",
    "simpleTableViewModalTransferStatus" : "Estado da Transferencia",
    "simpleTableViewModalSentOnDate" : "Envio efectuado em",
    "simpleTableViewModalAmountDetails": "Detalhes Monetarios",
    "simpleTableViewTotalSent": "Total Enviado",
    "simpleTableViewTotalWithTax": "Total com a Taxa de Envio",
    "simpleTableViewTotalReceived": "Total Recebido",
    "simpleTableViewTax": "Taxa",

    "simpleTableViewModalExitButton": "Fechar",

    //DEPOSITS
    "depositChooseType": "Escolha a Origem do Deposito"
}