const IndexController                       = require('./controllers/IndexController');
const LoginController                       = require('./controllers/LoginController');
const SignUpController                      = require('./controllers/SignUpController');
const DashBoardController                   = require('./controllers/DashboardController');
const CustomerOperationController           = require('./controllers/CustomerOperationsController');
const DevelopersController                  = require('./controllers/DevelopersController');
const MerchantsController                   = require('./controllers/MerchantsController');
const TransferController                    = require('./controllers/TransferController');
const SendController                        = require('./controllers/SendController');
const ProfileController                     = require('./controllers/ProfileController');

var Joi                     = require('joi');

module.exports = [{
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },
    {
        method: 'GET',
        path: '/emailchangecnf/{path}/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },
    {
        method: 'GET',
        path: '/twowayauth/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },
    {
        method: 'GET',
        path: '/',
        handler: IndexController.RenderDefault,
    },
    {   
        method: 'GET',
        path: '/developers',
        handler: DevelopersController.RenderDefault,
    },
    {   
        method: 'GET',
        path: '/merchants',
        handler: MerchantsController.RenderDefault,
    },
    {
        method: 'GET',
        path: '/signin',
        config: {
            handler: LoginController.LoginPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/twowayauth/{rpath}',
        config: {
            handler: LoginController.RenderTwoWaysAuthPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/twowayauthaction',
        config: {
            validate: {
                payload: {
                    token: Joi.string().required()
                }
            },
            handler: LoginController.ValidateTwoWaysToken,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/signout',
        config: {
            handler: LoginController.SignOut,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        //Regex password
        method: 'POST',
        path: '/authUser',
        config : {
            handler: LoginController.Login,
            validate: {
                payload: {
                    email : Joi.string().email(),
                    password : Joi.string().min(5).max(25),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/logout',
        handler: DashBoardController.LogOut
    },
    {
        method: 'GET',
        path: '/signup',
        handler: SignUpController.SignUpPagge
    },
    {
        //Regex password
        method: 'POST',
        path: '/reAuth',
        config : {
            handler: LoginController.ReLogin,
            validate: {
                payload: {
                    password : Joi.string().min(5).max(25),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/ceema',
        handler: SignUpController.CheckExistingEmail,
        config: {
            validate: {
                payload: {
                    email : Joi.string().email()
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/rgusr',
        handler: SignUpController.SignUp,
        config: {
            validate: {
                payload: {
                    email: Joi.string().email(),
                    password: Joi.string().min(6).max(20),
                    fname: Joi.string().min(3).max(50),
                    lname: Joi.string().min(3).max(50),
                    phone: Joi.string().required(),
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/actaccount',
        handler: SignUpController.ActivatePage,
        config: {
            validate : {
                params : {
                    email : Joi.string().email()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/signupsc',
        config: { 
            handler:SignUpController.SignUpSuccessPagge,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        } 
    },
    {
        method: 'GET',
        path: '/dashboard',
        config: {
            handler: DashBoardController.DashBoardPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/addbank',
        config: {
            handler: CustomerOperationController.RenderAddBank,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/chkbacc',
        config: {
            handler: CustomerOperationController.RenderViewBankAccount,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'PATCH',
        path: '/ptchbacc',
        config: {
            handler: CustomerOperationController.AddCustomerBankAccount,
            validate: {
                payload: {
                    bankname: Joi.string(),
                    accnumber: Joi.string(),
                    acciban: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/depositcc',
        config: {
            handler: CustomerOperationController.RenderDepositPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/choosedpt',
        config: {
            handler: CustomerOperationController.RenderChooseDepositType,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/chooseDpType',
        config: {
            handler: CustomerOperationController.ChooseDepositType,
            validate: {
                payload: {
                    type: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/getcrates',
        config: {
            handler: CustomerOperationController.GetRate,
            validate: {
                params: {
                    from: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/gdready',
        config: {
            handler: CustomerOperationController.GetDepositReady,
            validate: {
                payload: {
                    amt: Joi.string(),
                    amounttoget: Joi.string(),
                    dpdesc : Joi.string().min(0).max(50),
                    stdesc : Joi.string().min(0).max(50),
                    cardn : Joi.string(),
                    cardexp : Joi.string(),
                    cardcvv : Joi.string(),
                    currency : Joi.string(),
                    amtoeku : Joi.string()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/fndeposit',
        config: {
            handler: CustomerOperationController.RenderFinishDeposit,
            validate: {
                params: {
                    from: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/senddeposit',
        config: {
            handler: CustomerOperationController.SendDeposit,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/scdeposit',
        config: {
            handler: CustomerOperationController.RenderSuccessDeposit,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/sendmoney',
        config: {
            handler: SendController.RenderSendMoney,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/smsuccess',
        config: {
            handler: SendController.RenderSendMoneySuccess,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/sendmaction',
        config: {
            handler : SendController.SendMoney,
            validate: {
                payload: {
                    cardsn: Joi.string().min(16).max(16).required(),
                    amount: Joi.number().required(),
                    description: Joi.string().required()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/getevtdetail', 
        config : {
            handler: CustomerOperationController.GetEventDetails,
            validate: {
                params: {
                    did: Joi.string()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/inittransf',
        config: {
            handler: TransferController.InitTransferRenderPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/chcntytranf',
        config: {
            handler: TransferController.ChooseCountryForTransfer,
            validate: {
                payload: {
                    ccode: Joi.number(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/transferchbank',
        config: {
            handler: TransferController.ChooseBankForTransferRenderPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/chbknametranf',
        config: {
            handler: TransferController.ChooseBankNameForTransfer,
            validate: {
                payload: {
                    bankName: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/subtrasnfrdy',
        config: {
            handler: TransferController.SubmitTransferRenderPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/sendtransf',
        config: {
            handler: TransferController.SubmitTransferAction,
            validate: {
                payload: {
                    receiverName: Joi.string(),
                    accountNumber: Joi.string(),
                    description: Joi.string(),
                    amountWFee: Joi.number().min(0),
                    originalAmount: Joi.number().min(0),
                    amountToTransfer : Joi.number().min(0),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/transfsuccess',
        config: {
            handler: TransferController.RenderTransferSuccess,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'GET',
        path: '/allevents',
        config: {
            handler: DashBoardController.RenderAllEventsPage,
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/chsearchparam',
        config: {
            handler: DashBoardController.ChangeSearchValues,
            validate: {
                query: {
                    type: Joi.string().required(),
                    dateRange: Joi.string().required()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },
    {
        method: 'POST',
        path: '/changesmsconfirmation',
        config: {
            handler: DashBoardController.ChangeSMSConfirmation,
            validate: {
                payload: {
                    status: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'POST',
        path: '/changeblockstatus',
        config: {
            handler: DashBoardController.ChangeBlockStatus,
            validate: {
                payload: {
                    status: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/profile',
        config: {
            handler: ProfileController.RenderProfilePage
        }
    },

    {
        method: 'POST',
        path: '/requestchangephone',
        config: {
            validate: {
                payload: {
                    phone: Joi.string().required()
                }
            },
            handler: ProfileController.RequestChangePhoneNumber
        }
    },

    {
        method: 'POST',
        path: '/validatechangephone',
        config: {
            validate: {
                payload: {
                    phone: Joi.string().required(),
                    token: Joi.string().required()
                }
            },
            handler: ProfileController.ConfirmChangePhoneNumber
        }
    },   

    {
        method: 'POST',
        path: '/requestchangeemail',
        config: {
            validate: {
                payload: {
                    email: Joi.string().required()
                }
            },
            handler: ProfileController.RequestChangeEmail
        }
    },

    {
        method: 'GET',
        path: '/emailchangecnf/{dpath}/{email}',
        config: {
            handler: ProfileController.RenderRequestChangeEmailConfirmationPage
        }
    },

    {
        method: 'POST',
        path: '/validatechangemail',
        config: {
            validate: {
                payload: {
                    token: Joi.string().required()
                }
            },
            handler: ProfileController.ValidateRequestChangeEmail
        }
    },

    {
        method: 'POST',
        path: '/requestchangepassword',
        config: {
            validate: {
                payload: {
                    actualpswd: Joi.string().required()
                }
            },
            handler: ProfileController.RequestChangePassword
        }
    },

    {
        method: 'POST',
        path: '/validatechangepassword',
        config: {
            validate: {
                payload: {
                    token: Joi.string().required(),
                    newpassword: Joi.string().required()
                }
            },
            handler: ProfileController.ValidateChangePassword
        }
    },  

    {
        method: 'POST',
        path: '/changetwowaysauth',
        config: {
            handler: ProfileController.ChangeTwoWaysAuthStatus,
            validate: {
                payload: {
                    status: Joi.string(),
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/etopupdepsit',
        config: {
            handler: CustomerOperationController.RenderEtopUpDepositPage
        }
    },

    {
        method: 'POST',
        path: '/senddpfetop',
        config: {
            handler : CustomerOperationController.DepositFromEtopUpCard,
            validate: {
                payload: {
                    cardn: Joi.string().min(10).max(10).required(),
                    description: Joi.string().required()
                }
            },
            state: {
                parse: true, // parse and store in request.state
                failAction: 'error' // may also be 'ignore' or 'log'
            }
        }
    },

    {
        method: 'GET',
        path: '/etopupsucess',
        config: {
            handler: CustomerOperationController.RenderSuccessEtopUpDepositPage
        }
    },

    {
        method: 'PATCH',
        path: '/changelanguage',
        config: {
            validate: {
                payload: {
                    lang: Joi.string().required()
                }
            },
            handler: IndexController.ChangeLanguage
        }
    }
]

