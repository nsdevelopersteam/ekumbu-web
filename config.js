const EXPOSED_PORT                  = process.env.EXPOSED_PORT;
const BASE_URL                      = process.env.BASE_URL;
const API_VERSION                   = process.env.API_VERSION;
const API_URL                       = process.env.API_URL;
const API_KEY                       = process.env.API_KEY;
const API_LIVE_MODE                 = process.env.API_LIVE_MODE

module.exports = {
    server: {
        staging: 'http://stagingweb.ekumbu.com',
        development: 'http://localhost:3005',
        production: 'https://ekumbu.com',
        port: EXPOSED_PORT || 3005,
        baseurl: BASE_URL || 'http://stagingweb.ekumbu.com',
    },
    API: {
        version: API_VERSION || '0.1.0',
        baseurl : API_URL || 'http://localhost:3000',
        basedevurl: 'http://localhost:3000',
        key : API_KEY || 'Bearer 345634252492049028459294910950205912934DFHE244',
        livemode : API_LIVE_MODE || 'false'
    },
    mail: {
        ekumbumail : "mail.ekumbu.com",
        mailkey : "key-6c9a2780e8f0a9a0976bd6938e0ffb23"
    },
    rates : {
        API : 'http://kwerapi.ekumbu.com/kwanzaer/getrate'
    },
    deposits : {
        fee : 5,
        url: 'http://stagingdeposit.ekumbu.com',
        access_key: 'Bearer deposit-access-token'
    },
    sendmoney : {
        url: 'http://stagingsendmoney.ekumbu.com',
        access_key: 'Bearer send-access-token'
    },
    documents : {
        url : 'http://testapidocuments.ekumbu.com'
    },
    utils: {
        randongenerate: '0123456789abcdefghijklmnopqrstuvxywz'
    },
    countriesToSend : [{
        index: 0,
        name : 'Angola',
        flag: 'angola.png',
        fee: 0,
        currency : 'AOA',
    },
    {
        index: 1,
        name: 'Brasil',
        flag: 'brazil.png',
        fee: 3,
        currency : 'BRL',
    },
    {
        index: 2,
        name: 'Portugal',
        flag: 'portugal.png',
        fee: 3,
        currency : 'EUR',
    },
    {
        index: 3,
        name: 'Estados Unidos da America',
        flag: 'usa.png',
        fee: 3,
        currency : 'USD',
    }],
    BanksByCountry : {
        "0": {
            'banks' : [{
                name: 'Banco Angolano de Investimentos, S.A.',
                logo: 'bai.jpg'
            },
            {
                name : 'Banco Angolano de Negócios e Comércio, S.A.',
                logo : 'banc.jpg'
            },
            {
                name : 'Banco BAI Microfinanças, SA.',
                logo : 'bmf.jpg'
            },
            {
                name: 'Banco BIC, S.A.',
                logo : 'bic.jpg'
            },
            {
                name : 'Banco de Fomento Angola, S.A.',
                logo : 'bfa.jpg'
            },
            {
                name : 'Banco de Poupanca e Credito, S.A.',
                logo : 'bpc.jpg'
            },
            {
                name : 'Banco de Comércio e Indústria, S.A.',
                logo : 'bci.jpg'
            },
            {
                name : 'Banco Sol, S.A.',
                logo : 'bsol.jpg'
            }]
        },
        "1" : {
            'banks' : [{
                name: 'Banco do Brasil',
                logo: 'bb.png'
            },
            {
                name: 'Banco Caixa Geral',
                logo: 'bancocaixageral.png'
            }]
        },
        "2" : {
            'banks' : [{
                name: 'Millennium BCP',
                logo: 'millenniumbcp.png'
            },
            {
                name: 'Banco Popular Portugues',
                logo: 'bancopopular.jpg'
            }]
        },
        "3" : {
            'banks' : [{
                name: 'JPMorgan Chase & Co.',
                logo: 'chase.png'
            },
            {
                name : 'Bank of America Corp.',
                logo: 'bankofamerica.png'
            },
            {
                name: 'Citigroup Inc.',
                logo: 'citybank.png'
            },
            {
                name: 'Wells Fargo & Co.',
                logo : 'wellsfargo.png'
            }]
        }
    }
};